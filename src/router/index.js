import Vue from 'vue'
import Router from 'vue-router'
import AppLayout from '../components/layouts/app/AppLayout'
import AuthLayout from '../components/layouts/auth/AuthLayout'
import lazyLoading from './lazyLoading'

Vue.use(Router)

const EmptyParentComponent = {
  template: '<router-view></router-view>'
}

const router = new Router({
  routes: [
    {
      path: '*',
      redirect: { name: 'exception404' }
    },
    {
      path: '/auth',
      component: AuthLayout,
      children: [
        {
          name: 'login',
          path: 'login',
          component: lazyLoading('auth/login/Login'),
          meta: {
            authCantAccess: true
          }
        },
        {
          name: 'register',
          path: 'register',
          component: lazyLoading('auth/register/Register'),
          meta: {
            authCantAccess: true
          }
        },
        {
          name: 'logout',
          path: 'logout',
          redirect: { name: 'login' }
        },
        {
          path: '',
          redirect: { name: 'exception404' }
        }
      ]
    },
    {
      path: '/exception',
      component: EmptyParentComponent,
      children: [
        {
          name: 'exception404',
          path: '404',
          component: lazyLoading('exceptions/404/404')
        },
        {
          path: '',
          redirect: { name: 'exception404' }
        }
      ]
    },
    {
      path: '/',
      component: AppLayout,
      children: [
        {
          name: 'dashboard',
          path: 'dashboard',
          component: lazyLoading('dashboard/Dashboard'),
          default: true,
          meta: {
            auth: ['dashboard.view']
          }
        },
        {
          path: 'data',
          component: EmptyParentComponent,
          children: [
            {
              name: 'data-axios',
              path: 'axios',
              component: lazyLoading('data/axios/Axios'),
              meta: {
                auth: ['data_axios.view']
              }
            },
            {
              name: 'data-table',
              path: 'table',
              component: lazyLoading('data/table/Table'),
              meta: {
                auth: ['data_table.view']
              }
            },
            {
              name: 'data-tableServerside',
              path: 'table-serverside',
              component: lazyLoading('data/table/TableServerside'),
              meta: {
                auth: ['data_table.view']
              }
            },
            {
              path: '',
              redirect: { name: 'exception404' }
            }
          ]
        },
        {
          name: 'form-data',
          path: 'form-data',
          component: lazyLoading('form-data/FormData'),
          meta: {
            auth: ['form_data.view']
          }
        },
        {
          name: 'alert',
          path: 'alert',
          component: lazyLoading('alert/Alert'),
          meta: {
            auth: ['alert.view']
          }
        },
        {
          path: 'user',
          component: EmptyParentComponent,
          children: [
            {
              name: 'user-profile',
              path: 'profile',
              component: lazyLoading('user/profile/Profile'),
              meta: {
                auth: []
              }
            },
            {
              path: '',
              redirect: { name: 'exception404' }
            }
          ]
        },
        {
          path: '',
          redirect: { name: 'dashboard' }
        }
      ]
    }
  ]
})

export default router
