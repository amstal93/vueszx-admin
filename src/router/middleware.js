const token = () => {
  if (localStorage.getItem('jwt')) {
    return true
  }

  return false
}

const permission = (requires, userPermission) => {
  let validPerm = false
  if (requires.length > 0) {
    for (let index in requires) {
      if (userPermission.includes(requires[index])) {
        validPerm = true
        break
      }
    }
  } else {
    validPerm = true
  }

  return validPerm
}

export {
  token,
  permission
}
