import { library } from '@fortawesome/fontawesome-svg-core'
import * as iconSolid from '@fortawesome/free-solid-svg-icons'
import * as iconRegular from '@fortawesome/free-regular-svg-icons'
import * as iconBrands from '@fortawesome/free-brands-svg-icons'

for (let icon in iconSolid) {
  if (icon.match(/fa[A-Z]{1}[a-zA-Z]*/g)) {
    library.add(iconSolid[icon])
  }
}

for (let icon in iconRegular) {
  if (icon.match(/fa[A-Z]{1}[a-zA-Z]*/g)) {
    library.add(iconRegular[icon])
  }
}

for (let icon in iconBrands) {
  if (icon.match(/fa[A-Z]{1}[a-zA-Z]*/g)) {
    library.add(iconBrands[icon])
  }
}
