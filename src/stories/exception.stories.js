/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import router from '@/router'
import AppException from '@/components/AppException'

storiesOf('Exception', module)
  .add('Using',
    () => ({
      router,
      components: {
        AppException
      },
      template: `
        <div class="text-center">
          <app-exception class="pb-5" number="404" text="Stories is here!"></app-exception>
        </div>
      `
    }),
    {
      info: '# Documentation'
    }
  )
