import axios from 'axios'
import store from 'vuex-store'

// create an axios instance
// axios.defaults.xsrfCookieName = 'csrftoken'
// axios.defaults.xsrfHeaderName = 'X-CSRFToken'

const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // api base url
  // timeout: 200000 ,// request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // set header token
    if (localStorage.getItem('token') != null) {
      config.headers['Authorization'] = 'Bearer ' + localStorage.getItem('token')
    }

    return config
  },
  error => {
    // alert modal
    store.commit('setAlert', {
      show: true,
      status: 'fail'
    })

    // request error
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  response => response,
  error => {
    // alert modal
    store.commit('setAlert', {
      show: true,
      status: 'fail'
    })

    // response error
    return Promise.reject(error.response)
  }
)

export default service
