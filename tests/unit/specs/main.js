import { shallowMount } from '@vue/test-utils'

import Vue from 'vue'
import router from '@/router'
import store from '@/store'
import '@/i18n'
import BootstrapVue from 'bootstrap-vue'
import ComponentPlugin from '@/components/_ComponentPlugin'
import VeeValidate from 'vee-validate'
import lodash from 'lodash'

/*
  ===== vue =====
*/
Vue.config.ignoredElements = [/^b-/, /^font-awesome-/, /apexchart/, /v-date-picker/, /fragment/, /vue-perfect-scrollbar/]

Vue.use(BootstrapVue)
Vue.use(ComponentPlugin)
Vue.use(VeeValidate, {
  inject: true,
  fieldsBagName: 'veeFields',
  errorBagName: 'veeErrors'
})

Vue.mixin({
  data () {
    return {
      lodash: lodash
    }
  },
  methods: {
    $api (request) {
      return new Promise(function (resolve, reject) {
        resolve(true)
      })
    },
    $validateState (ref) { // validate on change input field
      if (
        this.veeFields[ref] &&
        (this.veeFields[ref].dirty || this.veeFields[ref].validated)
      ) {
        return !this.veeErrors.has(ref)
      }
      return null
    },
    $validateStateFeedback (ref) {
      if (
        this.veeFields[ref] &&
        (this.veeFields[ref].dirty || this.veeFields[ref].validated)
      ) {
        return !this.veeErrors.has(ref) ? null : this.veeErrors.first(ref)
      }
      return null
    },
    $submitDelete (data) { // submit delete should be override in component
      return new Promise((resolve, reject) => {
        resolve(true)
      })
    },
    $confirmDelete (data) { // confirm delete modal
      this.$swal.fire({
        type: 'warning',
        title: this.$t('alert.submitDelete.title'),
        text: this.$t('alert.submitDelete.text'),
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: this.$t('alert.submitDelete.confirmButtonText')
      }).then((result) => {
        if (result.value) {
          if (data.length === 0) {
            // minimum select atleast one
            this.$store.commit('setAlert', {
              show: true,
              status: 'custom',
              type: 'warning',
              title: this.$t('alert.submitDelete.warning.title'),
              text: this.$t('alert.submitDelete.warning.text')
            })
            return
          }

          // call submitDelete to do what it should
          this.$submitDelete(data).then(() => {
            this.$store.commit('setAlert', {
              show: true,
              status: 'custom',
              type: 'success',
              title: this.$t('alert.submitDelete.done.title'),
              text: this.$t('alert.submitDelete.done.text')
            })
          })
        }
      })
    },
  },
})

/*
  ===== setup =====
*/
var wrapper
var vm

function setup (Component, options = {}) {
  wrapper = shallowMount(Component, { sync: false, router, store, ...options })
  vm = wrapper.vm
}

export {
  wrapper,
  vm,
  setup
}
