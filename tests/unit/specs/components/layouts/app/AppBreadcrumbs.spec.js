import Component from '@/components/layouts/app/app-breadcrumbs/AppBreadcrumbs'
import { vm, setup } from '../../../main'

const mockBreadcrumbs = {
  root: {
    name: 'dashboard',
    displayName: 'menu.home',
  },
  routes: [
    {
      name: 'dashboard',
      displayName: 'menu.dashboard',
    },
    {
      name: 'user',
      displayName: 'menu.user',
      disabled: true,
      children: [
        {
          name: 'user-profile',
          displayName: 'menu.userProfile',
        }
      ],
    }
  ]
}

describe('AppBreadcrumbs', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component, {
      propsData: {
        breadcrumbs: mockBreadcrumbs,
        currentRouteName: 'dashboard'
      }
    })
  })

  it('watch:$store:config.locale', async () => {
    vm.setItems = jest.fn()
    await vm.$store.commit('setLocale', 'th')
    expect(vm.setItems).toBeCalledTimes(1)
  })

  it('displayedCrumbs', async () => {
    // not found nested
    vm.findInNestedByName = (a, b) => {
      return []
    }
    await vm.displayedCrumbs()
    expect(vm.displayedCrumbs()).toEqual([])

    // found nested
    vm.findInNestedByName = (a, b) => {
      return [1]
    }
    await vm.displayedCrumbs()
    expect(vm.displayedCrumbs()).toEqual([1])
  })

  it('findInNestedByName', async () => {
    // not found nested
    expect(vm.findInNestedByName(vm.breadcrumbs.routes, 'not')).toHaveLength(0)

    // found nested
    expect(vm.findInNestedByName(vm.breadcrumbs.routes, 'dashboard').length).toBeGreaterThan(0)
  })

  it('setItems', async () => {
    // not found nested
    vm.currentRouteName = 'test'
    vm.setItems()
    expect(vm.items).toHaveLength(1)

    // found nested but same as home
    vm.currentRouteName = 'dashboard'
    vm.setItems()
    expect(vm.items).toHaveLength(1)

    // found nested
    vm.currentRouteName = 'user-profile'
    vm.setItems()
    expect(vm.items.length).toBeGreaterThanOrEqual(1)
  })
})
