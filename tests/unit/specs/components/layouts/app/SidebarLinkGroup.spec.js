import Component from '@/components/layouts/app/app-sidebar/components/SidebarLinkGroup'
import { wrapper, vm, setup } from '../../../main'

describe('SidebarLinkGroup', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component, {
      propsData: {
        menu: {
          id: 1,
          title: 'menu.dashboard',
          icon: 'home',
          to: 'dashboard',
        }
      }
    })
  })

  it('getChildrens', () => {
    // parent not have to
    let children = [
      {
        id: 1,
        title: 'parent',
        icon: 'user',
        children: [
          {
            id: 2,
            title: 'child',
            to: 'child',
          }
        ]
      },
    ]
    expect(vm.getChildrens(children)).toEqual(['child'])

    // parent have to
    children = [
      {
        id: 1,
        title: 'parent',
        icon: 'user',
        to: 'parent',
        children: [
          {
            id: 2,
            title: 'child',
            to: 'child',
          }
        ]
      },
    ]
    expect(vm.getChildrens(children)).toHaveLength(2)
    expect(vm.getChildrens(children).includes('parent')).toBeTruthy()
    expect(vm.getChildrens(children).includes('child')).toBeTruthy()
  })

  it('isCollapse', () => {
    const route = {
      name: 'user-profile'
    }

    // current menu is child of this menu
    wrapper.setData({
      showCollapse: false
    })
    vm.getChildrens = () => ['user-profile']
    vm.isCollapse(route)
    expect(vm.showCollapse).toBeTruthy()

    // current menu isn't child of this menu
    wrapper.setData({
      showCollapse: true
    })
    vm.getChildrens = () => []
    vm.isCollapse(route)
    expect(vm.showCollapse).toBeFalsy()
  })
})
