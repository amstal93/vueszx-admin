import Component from '@/components/layouts/app/app-navbar/dropdowns/NotificationDropdown'
import { setup } from '../../../main'

describe('NotificationDropdown', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component, {
      propsData: {
        options: [
          {
            name: 'test',
            details: { name: 'Unit test' },
          }
        ]
      }
    })
  })

  it('init', () => {
    'Noting to test'
  })
})
