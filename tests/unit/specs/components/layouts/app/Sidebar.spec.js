import Component from '@/components/layouts/app/app-sidebar/Sidebar'
import { setup } from '../../../main'

describe('Sidebar', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component)
  })

  it('init', () => {
    'Noting to test'
  })
})
