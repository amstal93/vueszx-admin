import Component from '@/components/AppTable'
import { wrapper, vm, setup } from '../main'
import { sleep } from '@/util/function'

describe('AppTable', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component, {
      propsData: {
        app: 'test',
        items: [{
          id: 1,
          isActive: true,
          age: 40,
          name: {
            first: 'Dickerson',
            last: 'Macdonald'
          }
        },
        {
          id: 2,
          isActive: false,
          age: 21,
          name: {
            first: 'Larsen',
            last: 'Shaw'
          }
        },
        {
          id: 3,
          isActive: false,
          age: 89,
          name: {
            first: 'Geneva',
            last: 'Wilson'
          }
        }
        ],
        fields: [{
          key: 'select',
          label: 'Select'
        },
        {
          key: 'name',
          label: 'Person Full name',
          sortable: true,
          sortDirection: 'desc'
        },
        {
          key: 'age',
          label: 'Person age',
          sortable: true,
          class: 'text-center'
        },
        {
          key: 'isActive',
          label: 'is Active'
        },
        {
          key: 'actions',
          label: 'Actions'
        }
        ],
        isBusy: false,
        filter: '',
        perPage: 10,
        currentPage: 1,
        totalRows: 3,
        hasUpdate: true,
        hasDelete: true
      }
    })
  })

  it('watch:sortBy', async () => {
    vm.emitServer = jest.fn()
    await wrapper.setData({
      sortBy: 'test'
    })
    expect(vm.emitServer).toBeCalledTimes(1)
  })

  it('watch:sortDesc', async () => {
    vm.emitServer = jest.fn()
    await wrapper.setData({
      sortDesc: true
    })
    expect(vm.emitServer).toBeCalledTimes(1)
  })

  it('watch:selected', async () => {
    wrapper.setData({
      select_all: true,
      stateSelected: [1]
    })

    // make selected not empty
    wrapper.setProps({
      selected: [1]
    })
    await vm.$nextTick(() => {
      expect(vm.select_all).toEqual(true)
      expect(vm.stateSelected).toHaveLength(1)
    })

    // make selected empty
    wrapper.setProps({
      selected: []
    })
    await vm.$nextTick(() => {
      expect(vm.select_all).toEqual(false)
      expect(vm.stateSelected).toHaveLength(0)
    })
  })

  it('changePage', async () => {
    vm.emitUpdateCurrentPage = jest.fn()
    vm.emitUpdateSelected = jest.fn()
    vm.emitServer = jest.fn()
    const page = 2

    await vm.changePage(page)
    expect(vm.emitUpdateCurrentPage).toBeCalledTimes(1)
    expect(vm.emitUpdateSelected).toBeCalledTimes(1)
    expect(vm.emitServer).toBeCalledTimes(1)
    expect(vm.stateCurrentPage).toEqual(page)
  })

  it('changePerPage', async () => {
    vm.emitUpdatePerPage = jest.fn()
    vm.emitUpdateSelected = jest.fn()
    vm.emitServer = jest.fn()
    const perPage = 10

    await vm.changePerPage(perPage)
    expect(vm.emitUpdatePerPage).toBeCalledTimes(1)
    expect(vm.emitUpdateSelected).toBeCalledTimes(1)
    expect(vm.emitServer).toBeCalledTimes(1)
    expect(vm.statePerPage).toEqual(perPage)
  })

  it('changeFilter', async () => {
    vm.emitUpdateFilter = jest.fn()
    vm.emitUpdateCurrentPage = jest.fn()
    vm.emitServer = jest.fn()
    const event = {
      target: {
        value: 'as'
      }
    }

    vm.changeFilter(event)
    await sleep(700)
    expect(vm.emitUpdateFilter).toBeCalledTimes(1)
    expect(vm.emitUpdateCurrentPage).toBeCalledTimes(1)
    expect(vm.emitServer).toBeCalledTimes(1)
  })

  it('tableFilter', async () => {
    vm.emitUpdateTotalRows = jest.fn()
    const items = []

    await vm.tableFilter(items)
    expect(vm.emitUpdateTotalRows).toBeCalledTimes(1)
  })

  it('toggleSelected', async () => {
    // seleted
    vm.emitUpdateSelected = jest.fn()
    await vm.toggleSelected(true, 1)
    expect(vm.emitUpdateSelected).toBeCalledTimes(1)

    // deselected
    vm.emitUpdateSelected = jest.fn()
    wrapper.setProps({
      selected: [1]
    })
    await vm.toggleSelected(false, 1)
    expect(vm.emitUpdateSelected).toBeCalledTimes(1)
  })

  it('selectAll', async () => {
    /*
    * not server side
    */
    wrapper.setProps({
      items: [
        { id: 1 },
        { id: 2 },
        { id: 3 },
        { id: 4 },
        { id: 5 },
        { id: 6 },
        { id: 7 },
        { id: 8 },
        { id: 9 },
        { id: 10 },
        { id: 11 }
      ]
    })
    vm.emitUpdateSelected = jest.fn()
    await vm.selectAll(true)
    expect(vm.stateSelected).toHaveLength(10)
    expect(vm.emitUpdateSelected).toBeCalledTimes(1)

    vm.emitUpdateSelected = jest.fn()
    await vm.selectAll(false)
    expect(vm.stateSelected).toHaveLength(0)
    expect(vm.emitUpdateSelected).toBeCalledTimes(1)

    /*
    * server side
    */
    wrapper.setProps({
      serverside: true
    })
    wrapper.setData({
      stateSelected: []
    })
    vm.emitUpdateSelected = jest.fn()
    await vm.selectAll(true)
    expect(vm.stateSelected).toHaveLength(11)
    expect(vm.emitUpdateSelected).toBeCalledTimes(1)

    vm.emitUpdateSelected = jest.fn()
    await vm.selectAll(false)
    expect(vm.stateSelected).toHaveLength(0)
    expect(vm.emitUpdateSelected).toBeCalledTimes(1)
  })

  it('emitUpdatePerPage', async () => {
    await vm.emitUpdatePerPage(20)
    expect(wrapper.emitted('update:perPage')).toBeTruthy()
  })

  it('emitUpdateTotalRows', async () => {
    await vm.emitUpdateTotalRows(20)
    expect(wrapper.emitted('update:totalRows')).toBeTruthy()
  })

  it('emitUpdateCurrentPage', async () => {
    await vm.emitUpdateCurrentPage(1)
    expect(wrapper.emitted('update:currentPage')).toBeTruthy()
  })

  it('emitUpdateFilter', async () => {
    await vm.emitUpdateFilter('a')
    expect(wrapper.emitted('update:filter')).toBeTruthy()
  })

  it('emitUpdateSelected', async () => {
    await vm.emitUpdateSelected([1])
    expect(wrapper.emitted('update:selected')).toBeTruthy()
  })

  it('emitServer', () => {
    let emit
    wrapper.setProps({
      serverside: false
    })
    vm.emitServer()
    emit = wrapper.emitted('server')
    expect(emit).toBeFalsy()

    wrapper.setProps({
      serverside: true,
    })
    vm.emitServer()
    emit = wrapper.emitted('server')
    expect(emit).toBeTruthy()
    expect(emit[emit.length - 1][0]).toEqual({
      filter: '',
      sortBy: '',
      sortDesc: 'asc',
      page: 1,
      perPage: 10
    })

    wrapper.setProps({
      serverside: true,
      filter: 'test',
      perPage: 10,
      currentPage: 1
    })
    wrapper.setData({
      sortBy: 'testSortBy',
      sortDesc: true
    })
    vm.emitServer()
    expect(emit).toBeTruthy()
    expect(emit[emit.length - 1][0]).toEqual({
      filter: 'test',
      sortBy: 'testSortBy',
      sortDesc: 'desc',
      page: 1,
      perPage: 10
    })
  })

  it('emitEdit', async () => {
    await vm.emitEdit({})
    expect(wrapper.emitted('edit')).toBeTruthy()
    expect(wrapper.emitted('edit')).toEqual([[{}]])
  })

  it('emitDelete', async () => {
    await vm.emitDelete({})
    expect(wrapper.emitted('delete')).toBeTruthy()
    expect(wrapper.emitted('delete')).toEqual([[{}]])
  })
})
