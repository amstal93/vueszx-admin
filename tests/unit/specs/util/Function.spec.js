import { asyncForEach, sleep } from '@/util/function'

describe('Function', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('asyncForEach', () => {
    let data = [1, 2, 3, 4, 5]
    let count = 0
    asyncForEach(data, value => {
      expect(data[count]).toEqual(value)
      count++
    })
  })

  it('sleep', async () => {
    let beforeSleep = (new Date()).getTime() + 100
    await sleep(100).then(() => {
      expect(beforeSleep + 2 < (new Date()).getTime() < beforeSleep + 2).toBeTruthy()
    })
  })
})
