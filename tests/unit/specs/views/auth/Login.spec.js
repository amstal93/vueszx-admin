import Component from '@/views/auth/login/Login'
import { vm, setup } from '../../main'

describe('Login', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component)
  })

  it('onSubmit', async () => {
    await vm.onSubmit()

    vm.$validator.validateAll = () => {
      return new Promise(resolve => {
        resolve(true)
      })
    }
    await vm.onSubmit()
  })
})
