import Component from '@/views/data/axios/Add'
import { wrapper, vm, setup } from '../../../main'

describe('Add-DataAxios', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component, {
      propsData: {
        submit: false,
        close: false
      }
    })
  })

  it('watch:submit', async () => {
    vm.onSubmit = jest.fn()
    wrapper.setProps({
      submit: true
    })
    await vm.$nextTick(() => {
      expect(vm.onSubmit).toBeCalledTimes(1)
    })

    vm.onSubmit = jest.fn()
    wrapper.setProps({
      submit: false
    })
    await vm.$nextTick(() => {
      expect(vm.onSubmit).toBeCalledTimes(0)
    })
  })

  it('watch:close', async () => {
    vm.onClose = jest.fn()
    wrapper.setProps({
      close: true
    })
    await vm.$nextTick(() => {
      expect(vm.onClose).toBeCalledTimes(1)
    })

    vm.onClose = jest.fn()
    wrapper.setProps({
      close: false
    })
    await vm.$nextTick(() => {
      expect(vm.onClose).toBeCalledTimes(0)
    })
  })

  it('addRow', async () => {
    let beforeLength = vm.formData.length
    await vm.addRow()
    expect(vm.formData.length).toEqual(beforeLength + 1)
  })

  it('removeRow have only one row', async () => {
    let beforeLength = vm.formData.length
    await vm.removeRow()
    expect(vm.formData.length).toEqual(beforeLength)
  })

  it('removeRow have row more than one', async () => {
    wrapper.setData({
      formData: [
        {
          quantity: null,
          title: null
        },
        {
          quantity: null,
          title: null
        }
      ]
    })
    let beforeLength = vm.formData.length

    await vm.removeRow()
    expect(vm.formData.length).toEqual(beforeLength - 1)
  })

  it('onSubmit', async () => {
    vm.$bvModal.hide = jest.fn()
    vm.$validator.validateAll = () => {
      return new Promise(resolve => {
        resolve(true)
      })
    }
    await vm.onSubmit()
    expect(wrapper.emitted('update:submit')).toBeTruthy()
    expect(vm.$bvModal.hide).toBeCalledTimes(1)

    vm.$bvModal.hide = jest.fn()
    vm.$validator.validateAll = () => {
      return new Promise(resolve => {
        resolve(false)
      })
    }
    await vm.onSubmit()
    expect(wrapper.emitted('update:submit')).toBeTruthy()
    expect(vm.$bvModal.hide).toBeCalledTimes(0)
  })

  it('onClose', async () => {
    wrapper.setData({
      formData: [
        {
          quantity: 1,
          title: 2
        }
      ]
    })
    await vm.onClose()
    expect(vm.formData).toHaveLength(1)
    expect(vm.formData[0]).toEqual({
      quantity: null,
      title: null
    })
    expect(wrapper.emitted('update:close')).toBeTruthy()
  })
})
