import Component from '@/views/data/table/Table'
import { vm, setup } from '../../../main'

describe('Table', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component)
  })

  it('edit', () => {
    const item = {
      id: 1
    }
    vm.edit(item)
    expect(vm.update_data).toEqual(item)
  })

  it('$submitDelete', async () => {
    let beforeLength = vm.items.length
    await vm.$submitDelete([1])
    expect(vm.items).toHaveLength(beforeLength - 1)
  })
})
