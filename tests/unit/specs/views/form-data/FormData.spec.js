import Component from '@/views/form-data/FormData'
import { vm, setup } from '../../main'
import moment from 'moment'

const bvModalEvt = {
  preventDefault: () => true
}

describe('FormData', async () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component)
  })

  it('currentDate', () => {
    vm.$store.commit('setLocale', 'gb')
    expect(vm.currentDate).toEqual(moment().format('MM/DD/YYYY'))

    vm.$store.commit('setLocale', 'th')
    expect(vm.currentDate).toEqual(moment().format('DD/MM/YYYY'))
  })

  it('onSubmit', async () => {
    vm.$bvModal.hide = jest.fn()
    vm.$validator.validateAll = () => {
      return new Promise(resolve => {
        resolve(true)
      })
    }
    await vm.onSubmit(bvModalEvt)
    expect(vm.$bvModal.hide).toBeCalledTimes(1)

    vm.$bvModal.hide = jest.fn()
    vm.$validator.validateAll = () => {
      return new Promise(resolve => {
        resolve(false)
      })
    }
    await vm.onSubmit(bvModalEvt)
    expect(vm.$bvModal.hide).toBeCalledTimes(0)
  })
})
