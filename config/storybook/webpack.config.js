module.exports = ({ config }) => {
  // config for storybook-addon-vue-info
  config.module.rules.push({
    test: /\.vue$/,
    loader: 'storybook-addon-vue-info/loader',
    enforce: 'post'
  })

  return config
}
