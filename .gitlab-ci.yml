stages:
  - build
  - test
  - deploy
  - dast
  - performance

variables:
  DOCKER_DRIVER: overlay2
  # docker compose
  SERVICE: vueszx-admin
  IMAGE: $CI_REGISTRY_IMAGE
  TAG: latest
  CONTAINER_NAME: vueszx.admin
  HOSTNAME: vueszx-admin
  # --------------------------------
  DIR_DOCKER_LIVE: docker/live
  DIR_DOCKER_STORYBOOK: docker/storybook

default:
  image: johnnii/docker-executor
  services:
    - docker:dind
  before_script:
    # variables
    - export VERSION=$(echo ${CI_COMMIT_REF_NAME} | sed -n -e 's/^.*release\///p')
    # ssh agent
    - eval "$(ssh-agent -s)"
    - mkdir -p ~/.ssh
    - echo -e "-----BEGIN OPENSSH PRIVATE KEY-----\n${SSH_PRIVATE_KEY}\n-----END OPENSSH PRIVATE KEY-----" | tr -d '\r' | ssh-add - >/dev/null
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - chmod 700 ~/.ssh
    - docker info
    # login registry
    - docker login -u ${CI_REGISTRY_USER} -p${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
  after_script:
    - docker logout ${CI_REGISTRY}

# ========== template ==========
include:
  - template: Code-Quality.gitlab-ci.yml
  - template: Container-Scanning.gitlab-ci.yml
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: SAST.gitlab-ci.yml
  - template: DAST.gitlab-ci.yml
  - template: Verify/Browser-Performance.gitlab-ci.yml

code_quality:
  before_script:
    - echo Code Quality
  after_script:
    - echo END
  artifacts:
    paths: [gl-code-quality-report.json]
  tags:
    - docker

container_scanning:
  variables:
    CI_APPLICATION_REPOSITORY: $CI_REGISTRY_IMAGE
    CI_APPLICATION_TAG: $CI_COMMIT_SHORT_SHA
  before_script:
    - echo Container Scanning
  after_script:
    - echo END
  artifacts:
    reports:
      container_scanning: gl-container-scanning-report.json
  tags:
    - docker

dependency_scanning:
  before_script:
    - echo Dependency Scanning
  after_script:
    - echo END
  artifacts:
    reports:
      dependency_scanning: gl-dependency-scanning-report.json
  tags:
    - docker

sast:
  variables:
    SAST_GOSEC_LEVEL: 2
  before_script:
    - echo SAST
  after_script:
    - echo END
  artifacts:
    reports:
      sast: gl-sast-report.json
  tags:
    - docker

dast:
  variables:
    DAST_WEBSITE: https://vue.biszx.com
    DAST_AUTH_URL: https://vue.biszx.com/#/auth/login
    DAST_USERNAME: john.doe@example.com
    DAST_PASSWORD: john-doe-password
    DAST_AUTH_EXCLUDE_URLS: https://vue.biszx.com/#/auth/login,https://vue.biszx.com/#/auth/register
  before_script:
    - echo DAST
  after_script:
    - echo END
  artifacts:
    reports:
      dast: gl-dast-report.json
  tags:
    - docker

performance:
  variables:
    URL: https://vue.biszx.com
  before_script:
    - echo Performance
  after_script:
    - echo END
  tags:
    - docker

# ========== build ==========
build_run_test:
  stage: build
  script:
    - export IMAGE=${IMAGE}/test
    - docker build -f ${DIR_DOCKER_LIVE}/Dockerfile-npm -t ${IMAGE}:${TAG} .
    - docker push ${IMAGE}:${TAG}
  tags:
    - docker

build_commit:
  stage: build
  script:
    - export TAG=$CI_COMMIT_SHORT_SHA
    - docker build -f ${DIR_DOCKER_LIVE}/Dockerfile -t ${IMAGE}:${TAG} --build-arg BUILD=staging .
    - docker push ${IMAGE}:${TAG}
  tags:
    - docker

build_latest:
  stage: build
  script:
    - docker build -f ${DIR_DOCKER_LIVE}/Dockerfile -t ${IMAGE}:${TAG} --build-arg BUILD=staging .
    - docker push ${IMAGE}:${TAG}
  only:
    - develop
  tags:
    - docker

build_version:
  stage: build
  script:
    - export TAG=$VERSION
    - docker build -f ${DIR_DOCKER_LIVE}/Dockerfile -t ${IMAGE}:${TAG} --build-arg BUILD=production .
    - docker push ${IMAGE}:${TAG}
  only:
    - /^release\/\d+\.\d+(\.\d+)*$/
  tags:
    - docker

build_storybook:
  stage: build
  script:
    - export IMAGE=${IMAGE}/storybook
    - docker build -f ${DIR_DOCKER_STORYBOOK}/Dockerfile -t ${IMAGE}:${TAG} .
    - docker push ${IMAGE}:${TAG}
  only:
    - develop
    - /^release\/\d+\.\d+(\.\d+)*$/
  tags:
    - docker


# ========== test ==========
unit_test:
  stage: test
  script:
    - export IMAGE=${IMAGE}/test
    - docker run --rm ${IMAGE}:${TAG} yarn test:unit
  tags:
    - docker

e2e_test:
  stage: test
  script:
    - docker network create main-network || true

    # run server for test
    - export SERVICE=vueszx-admin-test
    - export TAG=$CI_COMMIT_SHORT_SHA
    - export CONTAINER_NAME=vueszx.admin.test
    - export HOSTNAME=vueszx-admin-test
    - envsubst < ${DIR_DOCKER_LIVE}/docker-compose-template.yml > ${DIR_DOCKER_LIVE}/docker-compose.yml
    - docker-compose -f ${DIR_DOCKER_LIVE}/docker-compose.yml up -d

    # run test
    - export SERVICE=vueszx-admin-e2e
    - export IMAGE=${IMAGE}/test
    - export TAG=latest
    - export CONTAINER_NAME=vueszx.admin.e2e
    - export HOSTNAME=vueszx-admin-e2e
    - envsubst < ${DIR_DOCKER_LIVE}/docker-compose-template.yml > ${DIR_DOCKER_LIVE}/docker-compose-e2e.yml
    - docker-compose -f ${DIR_DOCKER_LIVE}/docker-compose-e2e.yml run --rm -v $PWD/tests/e2e/report-html:/usr/src/app/tests/e2e/report-html vueszx-admin-e2e yarn report:e2e

    # remove server for test
    - docker-compose -f ${DIR_DOCKER_LIVE}/docker-compose.yml down
  artifacts:
    paths: [tests/e2e/report-html]
  tags:
    - docker


# =========== deploy ===========
deploy_latest:
  stage: deploy
  variables:
    DOCKER_HOST: ssh://szx@biszx.com:1804
  script:
    - docker network create main-network || true

    - envsubst < ${DIR_DOCKER_LIVE}/docker-compose-template.yml > ${DIR_DOCKER_LIVE}/docker-compose.yml
    - docker-compose -f ${DIR_DOCKER_LIVE}/docker-compose.yml up -d
  only:
    - develop
  tags:
    - docker

deploy_version:
  stage: deploy
  variables:
    DOCKER_HOST: ssh://szx@biszx.com:1804
  script:
    - docker network create main-network || true

    - export TAG=$VERSION
    - envsubst < ${DIR_DOCKER_LIVE}/docker-compose-template.yml > ${DIR_DOCKER_LIVE}/docker-compose.yml
    - docker-compose -f ${DIR_DOCKER_LIVE}/docker-compose.yml up -d
  only:
    - /^release\/\d+\.\d+(\.\d+)*$/
  tags:
    - docker

deploy_storybook:
  stage: deploy
  variables:
    DOCKER_HOST: ssh://szx@biszx.com:1804
  script:
    - docker network create main-network || true

    - export SERVICE=vueszx-admin-storybook
    - export IMAGE=${IMAGE}/storybook
    - export CONTAINER_NAME=vueszx.admin.storybook
    - export HOSTNAME=vueszx-admin-storybook
    - envsubst < ${DIR_DOCKER_LIVE}/docker-compose-template.yml > ${DIR_DOCKER_LIVE}/docker-compose.yml
    - docker-compose -f ${DIR_DOCKER_LIVE}/docker-compose.yml up -d
  only:
    - develop
    - /^release\/\d+\.\d+(\.\d+)*$/
  tags:
    - docker
